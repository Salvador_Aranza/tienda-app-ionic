import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Product } from '../models/product';
import { ApiService } from '../services/api.service'

@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.page.html',
  styleUrls: ['./products-edit.page.scss'],
})
export class ProductsEditPage implements OnInit {

  id: number;
  data: Product;
  constructor(
    public activatedRoute: ActivatedRoute, 
    public router: Router,
    public apiService: ApiService
  ) {
    this.data = new Product();
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getItem(this.id).subscribe(response => {
      console.log(response);
      this.data = response;
    })
  }
  update() {
    //Update item by taking id and updated data object
    this.apiService.updateItem(this.id, this.data).subscribe(response => {
      this.router.navigate(['products-list']);
    })
  }

}
