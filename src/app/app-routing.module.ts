import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'products-create',
    loadChildren: () => import('./products-create/products-create.module').then( m => m.ProductsCreatePageModule)
  },
  {
    path: 'products-edit/:id',
    loadChildren: () => import('./products-edit/products-edit.module').then( m => m.ProductsEditPageModule)
  },
  {
    path: 'products-list',
    loadChildren: () => import('./products-list/products-list.module').then( m => m.ProductsListPageModule)
  },
  {
    path: 'products-detail',
    loadChildren: () => import('./products-detail/products-detail.module').then( m => m.ProductsDetailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'client',
    loadChildren: () => import('./client/client.module').then( m => m.ClientPageModule)
  },
  {
    path: 'car-products-list/:id',
    loadChildren: () => import('./car-products-list/car-products-list.module').then( m => m.CarProductsListPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
