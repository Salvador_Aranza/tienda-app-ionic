import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.page.html',
  styleUrls: ['./client.page.scss'],
})
export class ClientPage implements OnInit {
  id: number;
  productsData:any;
  userName:string;
  userId : string;
  constructor(public apiService: ApiService) {
    this.productsData=[];
  }

  ngOnInit() {
    this.getUserLogged();
  }
  ionViewWillEnter() {
    // Used ionViewWillEnter as ngOnInit is not 
    // called due to view persistence in Ionic
    this.getAllProducts();
  }

  getAllProducts() {
    //Get saved list of products
    this.apiService.getList().subscribe(response => {
      console.log(response);
      this.productsData = response;
    })
  }
  add(item) {
    const car = { product_id: item.id, user_id: this.userId};
   console.log(item.id+" "+this.userId);
   this.apiService.addCar(car).subscribe(response=>{
    console.log("carrito: "+response);
  })
  }
  getListCar(id){
    this.apiService.listCar(id).subscribe(response=>{
      console.log("lista en carrito: "+JSON.stringify(response));
    })
  }
  getUserLogged() {
    var id =this.apiService.getUserLogged();
    
    this.apiService.getUser(id).subscribe(user => {
      this.userName= user['name'];
      this.userId = user['id'];
      console.log("usuario: "+user['name']);
    });
    //console.log(JSON.stringify(this.apiService.getToken()));
  }
}
