import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarProductsListPageRoutingModule } from './car-products-list-routing.module';

import { CarProductsListPage } from './car-products-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarProductsListPageRoutingModule
  ],
  declarations: [CarProductsListPage]
})
export class CarProductsListPageModule {}
