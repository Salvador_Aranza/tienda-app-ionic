import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarProductsListPage } from './car-products-list.page';

const routes: Routes = [
  {
    path: '',
    component: CarProductsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarProductsListPageRoutingModule {}
