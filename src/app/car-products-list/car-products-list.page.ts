import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'; 
import { ApiService } from '../services/api.service'
import { Product } from '../models/product';
@Component({
  selector: 'app-car-products-list',
  templateUrl: './car-products-list.page.html',
  styleUrls: ['./car-products-list.page.scss'],
})
export class CarProductsListPage implements OnInit {
  id: number;
  respuesta: any;
  constructor(
    public activatedRoute: ActivatedRoute, 
    public router: Router,
    public apiService: ApiService
  ) { 

  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.listCar(this.id).subscribe(response => {
      console.log(JSON.stringify(response));
      this.respuesta = response;
    })
  }

 
}
