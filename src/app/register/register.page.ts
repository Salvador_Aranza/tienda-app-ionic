import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  name:string;
  email: string;
  password: string;
  confirmPassword: string;

  constructor(public apiService: ApiService, public router: Router) { }

  ngOnInit() {
  }

  register() {
    const user = { name: this.name , email: this.email, password: this.password, c_password: this.confirmPassword };
    this.apiService.register(user).subscribe(data => {
      this.apiService.setToken(data.data.token);
      if (data.data.role==0) {
        this.router.navigateByUrl('client');
      }else{
        this.router.navigateByUrl('products-list');
      }
      
      //console.log(data);
    });
  }

}
