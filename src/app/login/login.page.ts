import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string;
  password: string;

  constructor(public apiService: ApiService, public router: Router) { }

  ngOnInit() {
    
  }
  login() {
    const user = {email: this.email, password: this.password};
    this.apiService.login(user).subscribe( data => {
      this.apiService.setToken(data.data);
      if (data.data.role==0) {
        this.router.navigateByUrl('client');
      }else{
        this.router.navigateByUrl('products-list');
      }
     
      console.log(data.data);
    });
  }

}
