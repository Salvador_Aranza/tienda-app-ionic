import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Product } from '../models/product';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { CookieService } from "ngx-cookie-service";


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // API path
  base_path = 'http://127.0.0.1:8000/';
  constructor(private http: HttpClient,private cookies: CookieService) { }
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
  // Get products data
  getList(): Observable<Product> {
    return this.http
      .get<Product>(this.base_path+'api/products/index')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  
  // Create a new item
  createItem(item): Observable<Product> {
    return this.http
      .post<Product>(this.base_path+'api/products/save', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
   // Get single product data by ID
   getItem(id): Observable<Product> {
    return this.http
      .get<Product>(this.base_path+'api/products/show' + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

   // Update item by id
   updateItem(id, item): Observable<Product> {
    return this.http
      .put<Product>(this.base_path+'api/products/update' + '/' + id, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  // Delete item by id
  deleteItem(id) {
    return this.http
      .delete<Product>(this.base_path+'api/products/delete' + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  login(user): Observable<any> {
    return this.http.post(this.base_path+'api/login', user);
  }

  register(user): Observable<any> {
    return this.http.post(this.base_path+'api/register', user);
  }
  setToken(token) {
    this.cookies.set("token", token.id);
    
  }
  getToken() {
    var token = this.cookies.get("token");
    return token;
  }

  getUser(id) {
    return this.http.get(this.base_path+"api/users/"+id);
  }

  getUserLogged() {
    const token = this.getToken();
    // Aquí iría el endpoint para devolver el usuario para un token
    return token;
  }

  addCar(car){
    return this.http
      .post(this.base_path+'api/cars/save', JSON.stringify(car), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  listCar(id){
    return this.http
    .get(this.base_path+'api/cars/index'+"/"+id)
    .pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
}
