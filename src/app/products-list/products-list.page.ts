import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.page.html',
  styleUrls: ['./products-list.page.scss'],
})
export class ProductsListPage implements OnInit {

  productsData:any;
  userName:string;
  userId : string;
  constructor(public apiService: ApiService) {
    this.productsData=[];
  }

  ngOnInit() {
    this.getUserLogged();
  }
  ionViewWillEnter() {
    // Used ionViewWillEnter as ngOnInit is not 
    // called due to view persistence in Ionic
    this.getAllProducts();
  }

  getAllProducts() {
    //Get saved list of products
    this.apiService.getList().subscribe(response => {
      console.log(response);
      this.productsData = response;
    })
  }
  delete(item) {
    //Delete item in Student data
    this.apiService.deleteItem(item.id).subscribe(Response => {
      //Update list after delete is successful
      this.getAllProducts();
    });
  }


  getUserLogged() {
    var id =this.apiService.getUserLogged();
    this.apiService.getUser(id).subscribe(user => {
      this.userName= user['name'];
      this.userId = user['id'];
      console.log("usuario: "+user);
    });
    //console.log(JSON.stringify(this.apiService.getToken()));
  }
}
